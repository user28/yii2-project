<?php

namespace app\models\forms;

class ImageUpload extends \yii\base\Model {

    public $image;

    public function rules() {
        return [
            [['image'], 'required'],
            [['image'], 'file', 'extensions' => 'jpg,png']
        ];
    }

    public function attributeLabels() {
        return [
            'image' => 'Изображение',
        ];
    }

    public function uploadFile(\yii\web\UploadedFile $file, $currentImage) {

        $this->image = $file;

        if ($this->validate()) {
            $this->deleteCurrentImage($currentImage);
            return $this->saveImage();
        }
    }

    private function getFolder() {
        return \Yii::getAlias('@web') . 'uploads/';
    }

    private function generateFileName() {
        return strtolower(md5(uniqid($this->image->baseName)) . '.' . $this->image->extension);
    }

    public function deleteCurrentImage($file) {
        if ($this->fileExists($file))
            unlink($this->getFolder() . $file);
    }

    private function fileExists($currentImage) {
        if (!empty($currentImage) && @$currentImage != null) {
            return file_exists($this->getFolder() . $currentImage);
        }
    }

    private function saveImage() {
        $filename = $this->generateFileName();

        $this->image->saveAs($this->getFolder() . $filename);

        return $filename;
    }

}