<?php
/**
 * Created by PhpStorm.
 * User: yurac
 * Date: 30.01.2019
 * Time: 15:50
 */

namespace app\models\forms;


use app\models\user\User;
use yii\base\Model;

class SignupForm extends Model {

    public $name;
    public $email;
    public $password;

    public function rules() {
        return [
            [['name', 'email', 'password'], 'required'],
            [['name'], 'string'],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass' => 'app\models\User', 'targetAttribute' => 'email']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'Электронная почта',
            'password' => 'Пароль',
        ];
    }

    public function signup() {
        if ($this->validate()) {
            $user = new User();


            $user->attributes = $this->attributes;

            return $user->create();
        }
    }

}