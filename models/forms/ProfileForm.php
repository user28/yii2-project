<?php
/**
 * Created by PhpStorm.
 * User: yurac
 * Date: 30.01.2019
 * Time: 17:21
 */

namespace app\models\forms;


use yii\base\Model;

class ProfileForm extends Model {
    public $id;
    public $name;
    public $email;

    public function rules() {
        return [
            [['name', 'email'], 'required'],
            [['email'], 'email']
        ];
    }

    public function attributeLabels() {
       return [
           'name' => 'Имя пользователя',
           'email' => 'Электронная почта'
       ];
    }
}