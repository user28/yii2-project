<?php
/**
 * Created by PhpStorm.
 * User: yurac
 * Date: 30.01.2019
 * Time: 16:41
 */

namespace app\models\comment;


use yii\base\Model;

class CommentForm extends Model {
    public $comment;

    public function rules() {
        return [
            [['comment'], 'required'],
            [['comment'], 'string', 'length' => [3, 250]]
        ];
    }

    public function attributeLabels() {
        return [
            'comment' => 'Комментарий',
        ];
    }

    public function saveComment($article_id) {
        $comment = new Comment();

        $comment->text = $this->comment;
        $comment->user_id = \Yii::$app->user->id;
        $comment->article_id = $article_id;
        $comment->date = date('Y-m-d');

        return $comment->save();
    }
}