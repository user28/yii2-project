<?php
/**
 * Created by PhpStorm.
 * User: yurac
 * Date: 30.01.2019
 * Time: 20:33
 */

namespace app\controllers;


use app\models\article\Article;
use app\models\category\Category;
use app\models\forms\ImageUpload;
use app\models\forms\ProfileForm;
use app\models\tag\Tag;
use app\models\user\User;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

class UserController extends Controller {
    public function actionProfile() {

        $user = User::findOne(Yii::$app->user->id);

        $model = new ProfileForm();

        $model->attributes = $user->attributes;

        return $this->render('profile', ['model' => $model]);
    }

    public function actionSetImage() {
        $model = new ImageUpload;

        if (Yii::$app->request->isPost) {

            $user = User::findOne(Yii::$app->user->id);

            $file = UploadedFile::getInstance($model, 'image');

            if ($user->saveImage($model->uploadFile($file, $user->photo))) {
                return $this->redirect(['profile', 'id' => $user->id]);
            }
        }

        return $this->render('image', ['model' => $model]);
    }

    public function actionArticles($user_id) {
        $data = Article::getAllByUser(4, $user_id);

        $user = User::findOne($user_id);

        $popular = Article::getPopular();

        $categories = Category::getAll();

        $tags = Tag::getAll();

        return $this->render('articles', [
            'articles' => $data['articles'],
            'pages' => $data['pages'],
            'popular' => $popular,
            'categories' => $categories,
            'tags' => $tags,
            'user' => $user,
        ]);
    }

}