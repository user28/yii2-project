<?php

namespace app\controllers;

use app\models\article\Article;
use app\models\category\Category;
use app\models\comment\CommentForm;
use app\models\forms\ImageUpload;
use app\models\tag\Tag;
use app\models\user\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\UploadedFile;

class SiteController extends Controller {
    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($category_id = null) {

        $data = Article::getAll(4, $category_id);

        $popular = Article::getPopular();

        $categories = Category::getAll();

        $tags = Tag::getAll();

        return $this->render('index', [
            'articles' => $data['articles'],
            'pages' => $data['pages'],
            'popular' => $popular,
            'categories' => $categories,
            'tags' => $tags,
        ]);
    }

}
