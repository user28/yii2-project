<?php
/**
 * Created by PhpStorm.
 * User: yurac
 * Date: 31.01.2019
 * Time: 16:15
 */

namespace app\controllers;


use app\models\article\Article;
use app\models\category\Category;
use app\models\comment\CommentForm;
use app\models\forms\ImageUpload;
use app\models\tag\Tag;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\UploadedFile;

class ArticleController extends Controller {
    public function actionUpdate($id) {
        var_dump($id);
        die;
    }

    public function actionGet($id) {

        $article = Article::findOne($id);

        $popular = Article::getPopular();

        $categories = Category::getAll();

        $comments = $article->comments;
        $commentForm = new CommentForm();

        $tags = Tag::getAll();
        $articleTags = $article->tags;

        return $this->render('single', [
            'article' => $article,
            'articleTags' => $articleTags,
            'popular' => $popular,
            'categories' => $categories,
            'tags' => $tags,
            'comments' => $comments,
            'commentForm' => $commentForm,
        ]);
    }

    public function actionComment($id) {
        $model = new CommentForm();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->saveComment($id)) {
                return $this->redirect(['/article/get', 'id' => $id]);
            }
        }
    }

    public function actionAddArticle() {
        $model = new Article();

        $categories = ArrayHelper::map(Category::find()->all(), 'id', 'title');

        $tags = ArrayHelper::map(Tag::find()->all(), 'id', 'title');

        if (Yii::$app->request->isPost) {
            $category = Yii::$app->request->post('category');
            $tags = Yii::$app->request->post('tags');
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $model->saveCategory($category);
                $model->saveTags($tags);
                return $this->redirect(['/article/article-add-image', 'id' => $model->id]);

            }
        }

        return $this->render('create_article', [
            'model' => $model,
            'categories' => $categories,
            'tags' => $tags,
        ]);
    }

    public function actionArticleAddImage($id) {
        $model = new ImageUpload;

        if (Yii::$app->request->isPost) {

            $article = Article::findOne($id);

            $file = UploadedFile::getInstance($model, 'image');

            if ($article->saveImage($model->uploadFile($file, $article->image))) {
                return $this->redirect(['/article/get', 'id' => $article->id]);
            }
        }

        return $this->render('image', ['model' => $model]);
    }
}