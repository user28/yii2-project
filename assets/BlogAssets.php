<?php
/**
 * Created by PhpStorm.
 * User: yurac
 * Date: 30.01.2019
 * Time: 10:56
 */

namespace app\assets;


use yii\web\AssetBundle;

class BlogAssets extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "/public/css/default.css",
	    "/public/css/layout.css",
	    "/public/css/media-queries.css",
    ];
    public $js = [
        "public/js/modernizr.js",
//        "http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js",
        "public/js/jquery-migrate-1.2.1.min.js",
        "public/js/main.js"
    ];
    public $depends = [
    ];
}