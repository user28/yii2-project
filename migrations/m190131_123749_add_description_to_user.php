<?php

use yii\db\Migration;

/**
 * Class m190131_123749_add_description_to_user
 */
class m190131_123749_add_description_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'description', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190131_123749_add_description_to_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190131_123749_add_description_to_user cannot be reverted.\n";

        return false;
    }
    */
}
