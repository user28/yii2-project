<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

\app\assets\BlogAssets::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- Header
================================================== -->
<header id="top">

    <div class="row">

        <div class="header-content twelve columns">

            <h1 id="logo-text"><a href="/" title="">Статьи от А до Я.</a></h1>
            <p id="intro">Не придумал как назвать...</p>

        </div>

    </div>

    <nav id="nav-wrap">

        <div class="row">
            <ul id="nav" class="nav">
                <li class="current"><a href="/">Home</a></li>

                <?php if(Yii::$app->user->isGuest):?>
                    <li class="current"><a href="<?= Url::toRoute(['auth/login'])?>">Авторизаваться</a></li>
                    <li class="current"><a href="<?= Url::toRoute(['auth/signup'])?>">Регистрация</a></li>
                <?php else: ?>
                    <li class="current"><a href="<?= Url::toRoute(['user/profile'])?>">Профиль</a></li>
                    <li class="current"><a href="<?= Url::toRoute(['user/articles', 'user_id' => Yii::$app->user->id])?>">Мои статьи</a></li>
                    <li class="current"> <?= Html::beginForm(['/auth/logout'], 'post')
                    . Html::submitButton(
                        'Выйти (' . Yii::$app->user->identity->name . ')',
                        ['class' => 'current']
                    )
                . Html::endForm() ?> </li>
                <?php endif;?>
            </ul> <!-- end #nav -->
        </div>

    </nav> <!-- end #nav-wrap -->

</header> <!-- Header End -->


<?= $content ?>


<!-- Footer
================================================== -->
<footer>

    <div class="row">

        <div class="twelve columns">
            <ul class="social-links">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-github-square"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-flickr"></i></a></li>
                <li><a href="#"><i class="fa fa-skype"></i></a></li>
            </ul>
        </div>

        <div class="six columns info">

            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                nibh id elit.
            </p>

            <p>Lorem ipsum Sed nulla deserunt voluptate elit occaecat culpa cupidatat sit irure sint
                sint incididunt cupidatat esse in Ut sed commodo tempor consequat culpa fugiat incididunt.</p>

        </div>

        <div class="two columns">
            <h3 class="social">Navigate</h3>

            <ul class="navigate group">
                <li><a href="#">Home</a></li>
            </ul>
        </div>

    </div> <!-- End row -->

    <div id="go-top"><a class="smoothscroll" title="Back to Top" href="#top"><i class="fa fa-chevron-up"></i></a></div>

</footer> <!-- End Footer-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
