<div id="sidebar" class="four columns">

    <div class="widget widget_categories group">
        <h3>Categories.</h3>
        <ul>
            <?php foreach ($categories as $category): ?>
                <li><a href="<?= \yii\helpers\Url::toRoute(['/site/index', 'category_id' => $category->id])?>" title=""><?= $category->title; ?></a> (<?= $category->getArticlesCount(); ?>
                    )
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="widget widget_text group">
        <h3>Widget Text.</h3>

        <p>Lorem ipsum Ullamco commodo laboris sit dolore commodo aliquip incididunt fugiat esse dolor aute
            fugiat minim eiusmod do velit labore fugiat officia ad sit culpa labore in consectetur sint cillum
            sint consectetur voluptate adipisicing Duis irure magna ut sit amet reprehenderit.</p>

    </div>

    <div class="widget widget_tags">
        <h3>Post Tags.</h3>

        <div class="tagcloud group">
            <?php foreach ($tags as $tag): ?>
                <a href="#"><?= $tag->title; ?></a>
            <?php endforeach ?>

        </div>

    </div>

    <div class="widget widget_popular">
        <h3>Popular Post.</h3>

        <ul class="link-list">
            <?php foreach ($popular as $article): ?>
                <li><a href="<?= \yii\helpers\Url::toRoute(['/site/single', 'id' => $article->id])?>"><?= $article->title; ?></a></li>
            <?php endforeach; ?>

        </ul>

    </div>

</div> <!-- end sidebar -->