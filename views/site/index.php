<?php

/* @var $this yii\web\View */

use yii\widgets\LinkPager;

$this->title = 'Статьи от А до Я';
?>
<!-- Content
================================================== -->
<div id="content-wrap">

    <div class="row">


        <div id="main" class="eight columns">

            <?php foreach ($articles as $article): ?>

                <article class="entry">

                    <header class="entry-header">

                        <h2 class="entry-title">
                            <a href="<?= \yii\helpers\Url::toRoute(['/article/get/', 'id' => $article->id]); ?>"
                               title=""> <?= $article->title; ?> </a>
                        </h2>

                        <div class="entry-meta">
                            <ul>
                                <li><?= $article->getDate(); ?></li>
                                <span class="meta-sep">&bull;</span>
                                <li>
                                    <a href="<?= \yii\helpers\Url::toRoute(['/site/index', 'category_id' => $article->category->id]) ?>"
                                       title="" rel="category tag"><?= $article->category->title; ?></a></li>
                                <span class="meta-sep">&bull;</span>
                                <li><a href="<?= \yii\helpers\Url::toRoute(['/user/articles', 'user_id' => $article->user->id]) ?>"><?= $article->user->name; ?></a></li>
                            </ul>
                        </div>

                    </header>

                    <div class="entry-content">
                        <p><?= $article->description; ?></p>
                    </div>

                </article>
            <?php endforeach; ?>

            <?php if (!Yii::$app->user->isGuest): ?>
                <a href="<?= \yii\helpers\Url::toRoute(['/article/add-article/']); ?>">
                    <button class="btn btn-default">Написать статью</button>
                </a>
            <?php endif ?>

            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]); ?>
        </div> <!-- end main -->

        <?= $this->render('/partials/sidebar', [
            'popular' => $popular,
            'categories' => $categories,
            'tags' => $tags,
        ]); ?>

    </div> <!-- end row -->

</div> <!-- end content-wrap -->
