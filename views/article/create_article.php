<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\article\Article */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Написать статью';
?>
<div class="content-wrap">
    <div class="row">
        <div class="article-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'title')->textInput() ?>

            <?= $form->field($model, 'description')->textInput() ?>

            <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

            <?= Html::dropDownList('category', null,  $categories, ['class' => 'form-control'])?>

            <?= Html::dropDownList('tags', null,  $tags, ['class' => 'form-control', 'multiple' => true])?>

            <?= $form->field($model, 'date')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

