<!-- Content
   ================================================== -->
<div id="content-wrap">

    <div class="row">

        <div id="main" class="eight columns">

            <article class="entry">

                <header class="entry-header">

                    <h2 class="entry-title">
                        <?= $article->title; ?>
                    </h2>

                    <div class="entry-meta">
                        <ul>
                            <li><?= $article->getDate(); ?></li>
                            <span class="meta-sep">&bull;</span>
                            <li>
                                <a href="#" title="" rel="category tag"><?= $article->category->title; ?></a>
                            </li>
                            <span class="meta-sep">&bull;</span>
                            <?php if (Yii::$app->user->id == $article->user->id): ?>
                                <a href="<?= \yii\helpers\Url::toRoute(['/article/update/', 'id' => $article->id]); ?>">Редактировать</a>
                            <?php else: ?>
                                <li>
                                    <a href="<?= \yii\helpers\Url::toRoute(['/user/articles', 'user_id' => $article->user->id]) ?>"><?= $article->user->name; ?></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>

                </header>

                <div class="entry-content-media">
                    <div class="post-thumb">
                        <img src="<?= $article->getImage(); ?>">
                    </div>
                </div>

                <div class="entry-content">
                    <p class="lead">
                        <?= $article->content; ?>
                    </p>
                </div>

                <p class="tags">
                    <span>Tagged in </span>:
                    <?php foreach ($articleTags as $tag): ?>
                        <a href="#"><?= $tag->title; ?></a>
                    <?php endforeach; ?>

                </p>

            </article>

            <!-- Comments
        ================================================== -->
            <div id="comments">
                <?php if (!empty($comments)): ?>
                    <h3><?= count($comments) ?> comments</h3>

                    <!-- commentlist -->
                    <ol class="commentlist">


                        <?php foreach ($comments as $comment): ?>
                            <li class="depth-1">

                                <div class="avatar">
                                    <img width="50" height="50" class="avatar" src="<?= $comment->user->getImage() ?>"
                                         alt="">
                                </div>

                                <div class="comment-content">

                                    <div class="comment-info">
                                        <cite><?= $comment->user->name; ?></cite>

                                        <div class="comment-meta">
                                            <time class="comment-time"
                                                  datetime="<?= $comment->date ?>"><?= $comment->getDate(); ?>
                                            </time>
                                        </div>
                                    </div>

                                    <div class="comment-text">
                                        <p><?= $comment->text; ?></p>
                                    </div>

                                </div>

                            </li>

                        <?php endforeach ?>


                    </ol> <!-- Commentlist End -->

                <?php endif ?>
                <!-- respond -->
                <div class="respond">

                    <?php if (!Yii::$app->user->isGuest): ?>

                        <h3>Оставить комментарий</h3>

                        <!-- form -->
                        <?php $form = \yii\widgets\ActiveForm::begin([
                        'action' => ['site/comment', 'id' => $article->id],
                        'options' => ['class' => 'form-horizontal contantForm', 'role' => 'form']])
                        ?>
                        <fieldset>

                            <div class="message group">
                                <?= $form->field($commentForm, 'comment')->textarea(['class' => 'form-control', 'placeholder' => 'Ваш комментарий'])
                                ?>
                            </div>

                            <button type="submit" class="submit">Отправить</button>

                        </fieldset>
                        <?php \yii\widgets\ActiveForm::end(); ?>
                    <?php endif ?>
                    <!-- Form End -->

                </div> <!-- Respond End -->

            </div>  <!-- Comments End -->


        </div> <!-- main End -->

        <?= $this->render('/partials/sidebar', [
            'popular' => $popular,
            'categories' => $categories,
            'tags' => $tags,
        ]); ?>

    </div> <!-- end row -->

</div> <!-- end content-wrap -->