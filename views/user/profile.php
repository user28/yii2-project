<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Профиль';

?>
<div class="row">
    <p style="margin-bottom: 0; margin-top: 10px">
        <?= Html::a('Загрузить картинку', ['set-image', 'id' => $model->id]) ?>
    </p>

    <?php $form = ActiveForm::begin();
    ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'email')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
