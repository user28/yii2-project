<?php

/* @var $this yii\web\View */

use yii\widgets\LinkPager;

$this->title = 'Статьи ' . $user->name;
?>
<!-- Content
================================================== -->
<div id="content-wrap" style="padding-top: 0;">

    <div class="row">

        <div class="header-content twelve columns">

            <ol class="commentlist" style="margin-top: 0">
                <li class="depth-1">

                    <div class="avatar">
                        <img width="150" height="150" class="avatar" src="<?= $user->getImage()?>" alt="">
                    </div>

                    <div class="comment-content">

                        <div class="comment-info">
                            <cite><?= $user->name; ?></cite>
                        </div>

                        <div class="comment-text">
                            <p><?= $user->description; ?></p>
                            <p><?= $user->email; ?></p>
                        </div>

                    </div>

                </li>
            </ol>

        </div>

        <div id="main" class="eight columns">

            <?php foreach ($articles as $article): ?>

                <article class="entry">

                    <header class="entry-header">

                        <h2 class="entry-title">
                            <a href="<?= \yii\helpers\Url::toRoute(['/site/single/', 'id' => $article->id]); ?>"
                               title=""> <?= $article->title; ?> </a>
                        </h2>

                        <div class="entry-meta">
                            <ul>
                                <li><?= $article->getDate(); ?></li>
                                <span class="meta-sep">&bull;</span>
                                <li>
                                    <a href="<?= \yii\helpers\Url::toRoute(['/site/index', 'category_id' => $article->category->id]) ?>"
                                       title="" rel="category tag"><?= $article->category->title; ?></a></li>
                                <span class="meta-sep">&bull;</span>
                                <?php if (Yii::$app->user->id == $article->user->id): ?>
                                    <a href="<?= \yii\helpers\Url::toRoute(['/site/update-article/', 'id' => $article->id]); ?>">Редактировать</a>
                                <?php else: ?>
                                    <li><?= $article->user->name; ?></li>
                                <?php endif; ?>


                            </ul>
                        </div>

                    </header>

                    <div class="entry-content">
                        <p><?= $article->description; ?></p>
                    </div>

                </article>
            <?php endforeach; ?>

            <?php if (!Yii::$app->user->isGuest): ?>
                <a href="<?= \yii\helpers\Url::toRoute(['/site/add-article/']); ?>">
                    <button class="btn btn-default">Написать статью</button>
                </a>
            <?php endif ?>

            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]); ?>
        </div> <!-- end main -->

        <?= $this->render('/partials/sidebar', [
            'popular' => $popular,
            'categories' => $categories,
            'tags' => $tags,
        ]); ?>

    </div> <!-- end row -->

</div> <!-- end content-wrap -->
